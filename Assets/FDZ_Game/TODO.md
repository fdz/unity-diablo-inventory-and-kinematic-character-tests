# OBJECTIVE
Trying to do a generic character controller. With good handling of steps and slopes.

# References
https://assetstore.unity.com/packages/tools/physics/kinematic-character-controller-99131
https://forum.unity.com/threads/released-kinematic-character-controller.497979/#post-3262218
http://www.00jknight.com/blog/unity-character-controller

# MILESTONE N-1
- [ ] Movement on any surface
- [ ] Player WASD
- [ ] Player click
- [ ] NPC follow
- [ ] NPC waypoints

# BACKLOG | Things to be made into milestones
- [ ] Jumping
- [ ] Ledge grabbing
- [ ] Ledge climbing
- [ ] Ledge movement
- [ ] Root motion actions
- [ ] Climbing: BOTW, AC, Mirror's Edge, Tomb Raider, Uncharted