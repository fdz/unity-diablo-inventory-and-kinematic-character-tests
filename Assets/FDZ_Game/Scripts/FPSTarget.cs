﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
namespace FDZ.Game
{
    public class FPSTarget : MonoBehaviour
    {
        [Range(0, 4)] public int vSyncCount = 1;
        public int targetFrameRate = 60;

        void Update()
        {
            vSyncCount = QualitySettings.vSyncCount = Mathf.Clamp(vSyncCount, 0, 4);
            Application.targetFrameRate = targetFrameRate;
        }
    }
}
#endif