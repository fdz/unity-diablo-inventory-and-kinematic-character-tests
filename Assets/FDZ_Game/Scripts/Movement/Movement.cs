﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;
using System.Linq;
using UnityInput = UnityEngine.Input;
using FDZ.Attributes;

namespace FDZ.Game
{
	// TODO: Add character model
	// TODO: IK for feet
	// TODO: rotate upper body to look at mouse, var lookAtPosition = grounHit.point + characterHeight
	public class Movement : MonoBehaviourGizmos
	{
		public struct Input
		{
			public float horizontal;
			public float vertical;
		}

		[SerializeField, ReadOnly] bool isOnGround;
		[SerializeField, ReadOnly] bool isStable;
		public float friction = 20f;
		public float speed = 8f;
		public float gravity = 10f;
		public Input input;
		public LayerMask groundLayerMask;
		private Rigidbody rb;
		private CapsuleCollider capsuleCollider;
		private bool jump;

		private void Awake()
		{
			rb = GetComponent<Rigidbody>();
			capsuleCollider = GetComponent<CapsuleCollider>();
		}

		private void OnEnable()
		{
			FindObjectOfType<SimpleCamera>().target = this.transform;
		}

		private void Update()
		{
			if (UnityInput.GetKeyDown(KeyCode.Space))
			{
				jump = true;
			}
		}

		private void FixedUpdate()
		{
			rb.interpolation = RigidbodyInterpolation.Interpolate;
			if (rb.isKinematic)
			{
				rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
			}
			else
			{
				rb.useGravity = false;
				rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
				rb.constraints = RigidbodyConstraints.FreezeRotation;
			}
			if (jump)
			{
				jump = false;
				rb.velocity = new Vector3(rb.velocity.x, 10f, rb.velocity.z);
			}
			bool wasOnGrounded = isOnGround;
			List<RaycastHit> hits = new List<RaycastHit>();
			float mod = 1f;
			int rays = 20;
			float radius = capsuleCollider.radius - .05f;
			//do
			//{
			//	float d = capsuleCollider.center.y + .1f;
			//	Vector3 center = transform.position + capsuleCollider.center;
			//	//hits = Physics3DExt.RaycastAroundAll(radius * mod, rays, center, -transform.up, d, groundLayerMask.value, QueryTriggerInteraction.UseGlobal, x => Vector3.Distance(x.point, transform.position + Vector3.up * capsuleCollider.radius) <= capsuleCollider.radius + .05f);
			//	if (Physics.OverlapSphere(transform.position + Vector3.up * (capsuleCollider.radius - .02f), capsuleCollider.radius - .01f).Length > 1)
			//	{
			//		hits.Add(new RaycastHit());
			//	}
			//	mod -= .1f;
			//}
			//while (hits.Count == 0 && mod > 0f);
			//if (hits.Count > 0)
			RaycastHit groundHit;
			if (Physics.SphereCast(transform.position + transform.up * capsuleCollider.radius, capsuleCollider.radius - .01f, Vector3.down, out groundHit, capsuleCollider.radius))
			{
				isOnGround = true;
				var averageHit = Utils.AverageRaycastHit(hits);
				if (rb.isKinematic)
				{
					rb.velocity += -rb.velocity * (friction * Time.deltaTime);
					//rb.velocity += GetSlopedMoveDirection(Utils.GetMoveDirectionFromMainCamera(), .5f, Vector3.down, 2f) * (speed * Time.deltaTime);
					rb.velocity += Vector3.Project(Utils.GetMoveDirectionFromMainCamera(), groundHit.normal) * speed * Time.deltaTime;
					rb.velocity = Vector3.ClampMagnitude(rb.velocity, speed / friction);
				}
				else
				{
					rb.AddForce(-rb.velocity * friction);
					rb.AddForce(GetSlopedMoveDirection(Utils.GetMoveDirectionFromMainCamera(), .5f, Vector3.down, 2f) * speed);
				}
			}
			else
			{
				isOnGround = false;
				if (rb.isKinematic)
				{
					isStable = false;
					rb.velocity += new Vector3(-rb.velocity.x, 0f, -rb.velocity.z) * (friction * Time.deltaTime);
					rb.velocity += Utils.GetMoveDirectionFromMainCamera() * (speed * Time.deltaTime);
					rb.velocity += Vector3.down * (gravity * Time.deltaTime);
				}
				else
				{
					rb.AddForce(new Vector3(-rb.velocity.x, 0f, -rb.velocity.z) * friction);
					rb.AddForce(Utils.GetMoveDirectionFromMainCamera() * speed);
					rb.AddForce(Vector3.down * gravity);
				}
			}
			if (rb.isKinematic)
			{
				Vector3 nextPosition = transform.position + rb.velocity * Time.deltaTime;
				var stabilized = false;
				var colliders = Physics.OverlapBox(nextPosition, Vector3.one * 10f, Quaternion.identity, groundLayerMask.value);
				foreach (var otherCollider in colliders)
				{
					if (otherCollider != capsuleCollider && Physics.ComputePenetration(capsuleCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
					{
						Vector3 penetrationVector = direction * distance;
						//if (Vector3.Angle(penetrationVector, Vector3.up) < 90)
						//{
						//	stabilized = true;
						//	nextPosition += new Vector3(0f, System.Math.Sign(direction.y) * distance, 0f);
						//	rb.velocity = new Vector3(rb.velocity.y, 0f, rb.velocity.z);
						//	rb.velocity -= Vector3.Project(rb.velocity, -direction);
						//}
						//else
						{
							nextPosition += penetrationVector;
							rb.velocity -= Vector3.Project(rb.velocity, -direction);
							var otherRB = otherCollider.attachedRigidbody;
							if (otherRB != null)
							{
								//rb.velocity -= rb.velocity * Mathf.Max(0, otherRB.mass - 1) * Time.deltaTime;
								otherRB.AddForce(-direction * distance * 1000f);
								Debug.Log(otherRB.gameObject);
							}
						}
					}
				}
				if (stabilized)
				{
					isStable = true;
				}
				rb.MovePosition(nextPosition);
			}
		}

		private Vector3 GetSlopedMoveDirection(Vector3 moveDirection, float frontOffset, Vector3 rayDirection, float rayMaxDistance)
		{
			var centerOrigin = transform.position + transform.up;
			if (Physics.Raycast(centerOrigin, rayDirection, out RaycastHit centerHit, rayMaxDistance, groundLayerMask.value))
			{
				var frontOrigin = centerOrigin + moveDirection * frontOffset;
				if (Physics.Raycast(frontOrigin, rayDirection, out RaycastHit frontHit, rayMaxDistance, groundLayerMask.value))
				{
					return (frontHit.point - centerHit.point).normalized;
				}
			}
			return moveDirection;
		}
	}
}