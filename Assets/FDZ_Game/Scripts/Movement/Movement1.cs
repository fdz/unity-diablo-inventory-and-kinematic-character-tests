﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;
using System.Linq;
using UnityInput = UnityEngine.Input;
using FDZ.Attributes;

namespace FDZ.Game
{
	// TODO: Add character model
	// TODO: IK for feet
	// TODO: rotate upper body to look at mouse, var lookAtPosition = grounHit.point + characterHeight
	public class Movement1 : MonoBehaviourGizmos
	{
		public struct Input
		{
			public float horizontal;
			public float vertical;
		}

		[SerializeField, ReadOnly] bool isOnGround;
		[SerializeField, ReadOnly] bool isStable;
		public float friction = 20f;
		public float speed = 8f;
		public float gravity = 10f;
		public Input input;
		public LayerMask groundLayerMask;
		private Rigidbody rb;
		private CapsuleCollider capsuleCollider;
		private bool jump;

		private void Awake()
		{
			rb = GetComponent<Rigidbody>();
			capsuleCollider = GetComponent<CapsuleCollider>();
		}

		private void OnEnable()
		{
			FindObjectOfType<SimpleCamera>().target = this.transform;
		}

		private void Update()
		{
			if (UnityInput.GetKeyDown(KeyCode.Space))
			{
				jump = true;
			}
		}

		private void FixedUpdate()
		{
			rb.isKinematic = true;
			rb.useGravity = false;
			rb.interpolation = RigidbodyInterpolation.Interpolate;
			rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
			// SET VELOCITY 
			Vector3 targetVelocity = Utils.GetMoveDirectionFromMainCamera() * speed;
			targetVelocity.y = rb.velocity.y - gravity * Time.deltaTime;
			rb.velocity = targetVelocity;
			// MOVE
			Vector3 nextPosition = transform.position + rb.velocity * Time.deltaTime;
			var colliders = Physics.OverlapBox(nextPosition, Vector3.one * 10f, Quaternion.identity, groundLayerMask.value);
			foreach (var otherCollider in colliders)
			{
				if (otherCollider != capsuleCollider && Physics.ComputePenetration(capsuleCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
				{
					Vector3 penetrationVector = direction * distance;
					nextPosition += penetrationVector;
					rb.velocity -= Vector3.Project(rb.velocity, -direction);
					var otherRB = otherCollider.attachedRigidbody;
					if (otherRB != null)
					{
						//rb.velocity -= rb.velocity * Mathf.Max(0, otherRB.mass - 1) * Time.deltaTime;
						otherRB.AddForce(-direction * distance * 1000f);
						otherRB.AddForceAtPosition(-direction * distance * 1000f, nextPosition + capsuleCollider.center);
						Debug.Log(otherRB.gameObject);
					}
				}
			}
			rb.MovePosition(nextPosition);
		}

		private Vector3 GetSlopedMoveDirection(Vector3 moveDirection, float frontOffset, Vector3 rayDirection, float rayMaxDistance)
		{
			var centerOrigin = transform.position + transform.up;
			if (Physics.Raycast(centerOrigin, rayDirection, out RaycastHit centerHit, rayMaxDistance, groundLayerMask.value))
			{
				var frontOrigin = centerOrigin + moveDirection * frontOffset;
				if (Physics.Raycast(frontOrigin, rayDirection, out RaycastHit frontHit, rayMaxDistance, groundLayerMask.value))
				{
					return (frontHit.point - centerHit.point).normalized;
				}
			}
			return moveDirection;
		}
	}
}