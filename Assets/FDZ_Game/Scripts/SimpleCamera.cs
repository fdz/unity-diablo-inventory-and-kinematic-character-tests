﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Game
{
    // TODO: Camera Clip https://www.youtube.com/playlist?list=PL4CCSwmU04MjDqjY_gdroxHe85Ex5Q6Dy

    public enum UpdateMode
    {
        Update,
        FixedUpdate,
        LateUpdate,
    }

    [ExecuteInEditMode]
    public class SimpleCamera : MonoBehaviour
    {
        [SerializeField] private Transform pivot = null;
        [SerializeField] private Transform cam = null;

        [Space]
        public float followSpeed;
        public Transform target;

        [Space]
        public UpdateMode updateMode;
        public Vector2 offset;
        public Vector2 eulerAngles;

        private void Update()
        {
            if (updateMode == UpdateMode.Update && Application.isPlaying)
            {
                Tick();
            }
        }

        private void FixedUpdate()
        {
            if (updateMode == UpdateMode.FixedUpdate && Application.isPlaying)
            {
                Tick();
            }
        }

        private void LateUpdate()
        {
            if (updateMode == UpdateMode.LateUpdate || Application.isEditor && Application.isPlaying == false)
            {
                Tick();
            }
        }

        private void Tick()
        {
            if (target == null || pivot == null || cam == null)
            {
                return;
            }

            if (Application.isEditor && Application.isPlaying == false)
            {
                transform.position = target.position;
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * followSpeed);
            }

            pivot.localPosition = new Vector3(0f, offset.y, 0f);
            cam.localPosition = new Vector3(0f, 0f, offset.x);

            transform.eulerAngles = new Vector3(0f, eulerAngles.y, 0f);
            pivot.localEulerAngles = new Vector3(eulerAngles.x, 0f, 0f);
        }
    }
}
