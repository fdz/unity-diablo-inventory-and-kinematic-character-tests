﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game
{
    public class MoveRigidbody : MonoBehaviour
    {
        public enum MovementType { MovePosition, AddForce }

        public MovementType movementType;
        public ForceMode forceMode;
        public float speed = 10f;
        public float force = 10f;
        public float distanceToTarget = 0.1f;

        public Transform pointA;
        public Transform pointB;

        [SerializeField, ReadOnly] private Transform target;
        [SerializeField, ReadOnly] private Vector3 startPosition;
        [SerializeField, ReadOnly] private float maxDistance;

        Rigidbody rb;

        void Awake()
        {
            target = pointA;
            startPosition = transform.position;
            maxDistance = Vector3.Distance(transform.position, target.position);
            rb = GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            if (Vector3.Distance(startPosition, transform.position) >= maxDistance)
            {
                target = target == pointA ? pointB : pointA;
                startPosition = transform.position;
                maxDistance = Vector3.Distance(transform.position, target.position);
            }
            Vector3 direction = (target.position - transform.position).normalized;
            switch (movementType)
            {
                case MovementType.MovePosition:
                    rb.MovePosition(rb.position + direction * (Time.deltaTime * speed));
                    break;
                case MovementType.AddForce:
                    rb.AddForce(direction * force, forceMode);
                    break;
                default:
                    break;
            }
        }
    }
}
