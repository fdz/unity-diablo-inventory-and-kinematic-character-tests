## 1.1 (2020-04-20)
    - Added \link Drawing.Draw.Label2D Draw.Label2D\endlink which allows you to easily render text from your code.
        It uses a signed distance field font renderer which allows you to render crisp text even at high resolution.
        At very small font sizes it falls back to a regular font texture.
        \shadowimage{rendered/label2d.png}
    - Improved performance of drawing lines by about 5%.
    - Fixed a potential crash after calling the Draw.Line(Vector3,Vector3,Color) method.

## 1.0.2 (2020-04-09)
    - Breaking changes
        - A few breaking changes may be done as the package matures. I strive to keep these to as few as possible, while still not sacrificing good API design.
        - Changed the behaviour of \link Drawing.Draw.Arrow(float3,float3,float3,float) Draw.Arrow\endlink to use an absolute size head.
            This behaviour is probably the desired one more often when one wants to explicitly set the size.
            The default Draw.Arrow(float3,float3) function which does not take a size parameter continues to use a relative head size of 20% of the length of the arrow.
            \shadowimage{rendered/arrow_multiple.png}
    - Added \link Drawing.Draw.ArrowRelativeSizeHead Draw.ArrowRelativeSizeHead\endlink which uses a relative size head.
        \shadowimage{rendered/arrowrelativesizehead.png}
    - Added \link Drawing.DrawingManager.GetBuilder DrawingManager.GetBuilder\endlink instead of the unnecessarily convoluted DrawingManager.instance.gizmos.GetBuilder.
    - Added \link Drawing.Draw.CatmullRom(List<Vector3>) Draw.CatmullRom\endlink for drawing a smooth curve through a list of points.
        \shadowimage{rendered/catmullrom.png}
    - Made it easier to draw things that are visible in standalone games. You can now use for example Draw.ingame.WireBox(Vector3.zero, Vector3.one) instead of having to create a custom command builder.
        See \ref ingame for more details.

## 1.0.1 (2020-04-06)
    - Fix burst example scene not having using burst enabled (so it was much slower than it should have been).
    - Fix text color in the SceneEditor example scene was so dark it was hard to read.
    - Various minor documentation fixes.

## 1.0 (2020-04-05)
    - Initial release
