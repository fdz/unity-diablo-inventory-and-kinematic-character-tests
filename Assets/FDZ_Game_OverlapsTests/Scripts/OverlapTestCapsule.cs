﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Game.OverlapsTests
{
	public class OverlapTestCapsule : MonoBehaviour
	{
		CapsuleCollider capsuleCollider;
		public Collider[] overlaps;

		private void Awake()
		{
			capsuleCollider = GetComponent<CapsuleCollider>();
		}

		private void FixedUpdate()
		{
			Vector3 point0 = capsuleCollider.bounds.center - transform.rotation * capsuleCollider.center;
			Vector3 point1 = point0 + transform.up * capsuleCollider.height;
			Debug.DrawLine(point0, point1, Color.red);
			float radius = capsuleCollider.radius;
			overlaps = Physics.OverlapCapsule(point0, point1, radius);
		}
	}
}