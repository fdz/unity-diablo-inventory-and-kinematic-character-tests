﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Name
{
	public class BoxCast : MonoBehaviour
	{
		public RaycastHit[] hits;
		void FixedUpdate()
		{
			hits = Physics.BoxCastAll(transform.position, Vector3.one / 2, Vector3.down, Quaternion.identity, 3f);
			var str = "";
			foreach (var hit in hits)
			{
				if (hit.collider.gameObject == this.gameObject)
					continue;
				str += hit.collider.name + " at " + Vector3.Angle(hit.normal, Vector3.up) + ", ";
				Debug.DrawRay(hit.point, hit.normal, Color.red, .1f);
			}
			//if (str != "")
			//	Debug.Log(str);
		}
	}
}