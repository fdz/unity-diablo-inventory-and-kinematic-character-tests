﻿namespace UnityEngine
{
	public static partial class Extensions
	{
		public static Color Darker(this Color self, float amount)
		{
			return new Color(self.r - amount, self.g - amount, self.b - amount, self.a);
		}
		
		public static Color Lighter(this Color self, float amount)
		{
			return new Color(self.r + amount, self.g + amount, self.b + amount, self.a);
		}

		public static Color Alpha(this Color self, float alpha)
		{
			return new Color(self.r, self.g, self.b, alpha);
		}
	} // end of class
} // end of namespace
