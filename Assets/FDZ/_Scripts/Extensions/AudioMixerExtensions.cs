﻿namespace UnityEngine
{
	public static partial class Extensions
	{
		public static float GetFloat(this Audio.AudioMixer self, string key)
		{
			self.GetFloat(key, out var value);
			return value;
		}
	} // end of class
} // end of namespace
