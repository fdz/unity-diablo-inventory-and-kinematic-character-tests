﻿namespace UnityEngine
{
	public static partial class Extensions
	{
		public static T GetComponentUp<T>(this GameObject self) where T : Component
		{
			T found = null;
			var parent = self.transform.parent;
			while (parent != null && found == null)
			{
				found = parent.GetComponent<T>();
				parent = parent.parent;
			}
			return found;
		}
	} // end of class
} // end of namespace
