﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	// [CreateAssetMenu(fileName = "AudioClipExt", menuName = "FDZ/ScriptableObjects/AudioClipExt", order = 1)]
	public abstract class SoundClip : ScriptableObject
	{
		// public abstract AudioClip clip { get; }
		public abstract void PlayAtPoint(Vector3 position);
	} // end of class
} // end of namespace
