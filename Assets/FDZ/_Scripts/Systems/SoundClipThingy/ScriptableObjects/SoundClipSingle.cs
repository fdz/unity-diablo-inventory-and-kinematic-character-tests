﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	[CreateAssetMenu(fileName = "SoundClipSingle", menuName = "FDZ/ScriptableObjects/SoundClipSingle", order = 1)]
	public class SoundClipSingle : SoundClip
	{
		[SerializeField] AudioClip _clip = null;
		[SerializeField] UnityEngine.Audio.AudioMixerGroup _output = null;
		[SerializeField, Range(0, 1)] float _volume = 1f;
		[SerializeField, Range(0, 3)] float _pitch = 1f;
		[SerializeField, Range(0, 5000)] float _minDistance = 1f;
		[SerializeField, Range(0, 5000)] float _maxDistance = 500f;

		public AudioClip clip { get => _clip; }
		public UnityEngine.Audio.AudioMixerGroup output { get => _output; }
		public float volume { get => _volume; }
		public float pitch { get => _pitch; }
		public float minDistance { get => _minDistance; }
		public float maxDistance { get => _maxDistance; }


		public override void PlayAtPoint(Vector3 position)
		{
			AudioManager.singleton.PlayAtPoint(clip, output, position, volume, pitch, minDistance, maxDistance);
		}
	} // end of class
} // end of namespace
