﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FDZ
{
    public static class Utils
    {
        public static Vector3 Vector3RotateAroundPoint(Vector3 point , Vector3 pivot, Quaternion angle)
        {
            // https://answers.unity.com/questions/47115/vector3-rotate-around.html
            return angle * (point - pivot) + pivot;
        }
    public static float QuaternionSingedAngle(Quaternion a, Quaternion b)
    {
        // https://answers.unity.com/questions/26783/how-to-get-the-signed-angle-between-two-quaternion.html
        // get a "forward vector" for each rotation
        var fromRotation = a * Vector3.forward;
        var toRortation = b * Vector3.forward;
        // get a numeric angle for each vector, on the X-Z plane (relative to world forward)
        var angleA = Mathf.Atan2(fromRotation.x, fromRotation.z) * Mathf.Rad2Deg;
        var angleB = Mathf.Atan2(toRortation.x, toRortation.z) * Mathf.Rad2Deg;
        // get the signed difference in these angles
        return Mathf.DeltaAngle(angleA, angleB);
    }

    public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
    }

    public static void EventSystem_RaycastAll(Vector3 position, List<RaycastResult> results)
    {
        // https://answers.unity.com/questions/1009987/detect-canvas-object-under-mouse-because-only-some.html
        PointerEventData pointerData = new PointerEventData(EventSystem.current) { pointerId = -1 };
        pointerData.position = position;
        EventSystem.current.RaycastAll(pointerData, results);
    }

    public static Vector2 RotatePoint(Vector2 point, Vector2 center, float angle)
    {
        angle = (angle) * (Mathf.PI / 180f); // Convert to radians
        var rotatedX = Mathf.Cos(angle) * (point.x - center.x) - Mathf.Sin(angle) * (point.y - center.y) + center.x;
        var rotatedY = Mathf.Sin(angle) * (point.x - center.x) + Mathf.Cos(angle) * (point.y - center.y) + center.y;
        return new Vector2(rotatedX, rotatedY);
    }

    public static float SignedAngle(Vector3 a, Vector3 b)
    {
        // https://answers.unity.com/questions/181867/is-there-way-to-find-a-negative-angle.html
        var angle = Vector3.Angle(a, b);
        var cross = Vector3.Cross(a, b);
        if (cross.y < 0)
            angle = -angle;
        return angle;
    }

    public static int NearestMultiple(float value, float factor)
    {
        return (int)(System.Math.Round((value / (double)factor), System.MidpointRounding.AwayFromZero) * factor);
    }

    public static Vector3 Abs(Vector3 v3)
    {
        return new Vector3(Mathf.Abs(v3.x), Mathf.Abs(v3.y), Mathf.Abs(v3.z));
    }

    public static bool IsInsideSurfaceArc2D(Vector2 arcPosition, Vector2 arcUp, Vector2 otherPosition, float arcAngle)
    {
        var direction = (otherPosition - arcPosition);
        var angle = Vector2.Angle(arcUp, direction) * 2;
        // Debug.Log(angle);
        return angle < arcAngle;
    }

    public static Vector3 GetMoveDirectionFromMainCamera()
    {
        var direction = GetInputDirection();
        var cameraTransform = Camera.main.transform;
        var right = new Vector3(cameraTransform.right.x, 0f, cameraTransform.right.z).normalized;
        var forward = new Vector3(cameraTransform.forward.x, 0f, cameraTransform.forward.z).normalized;
        return (right * direction.x + forward * direction.y).normalized;
    }

    public static Vector3 GetMoveDirection(Transform directionHelper)
    {
        var direction = GetInputDirection();
        return (directionHelper.right * direction.x + directionHelper.forward * direction.y).normalized;
    }

    public static Vector3 GetMoveDirection()
    {
        var direction = GetInputDirection();
        return new Vector3(direction.x, 0f, direction.y).normalized;
    }

    public static Vector2 Vector2RotateAroundPoint(Vector2 point, Vector2 center, float angle)
    {
        // https://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
        angle = (angle) * (Mathf.PI / 180); // Convert to radians
        var rotatedX = Mathf.Cos(angle) * (point.x - center.x) - Mathf.Sin(angle) * (point.y - center.y) + center.x;
        var rotatedY = Mathf.Sin(angle) * (point.x - center.x) + Mathf.Cos(angle) * (point.y - center.y) + center.y;
        return new Vector2(rotatedX, rotatedY);
    }

    public static float GetAxis(KeyCode negative, KeyCode positive)
    {
        var axis = 0f;
        if (Input.GetKey(negative))
        {
            axis += 1f;
        }
        if (Input.GetKey(positive))
        {
            axis += 1f;
        }
        return axis;
    }
    public static float GetAxisDown(KeyCode negative, KeyCode positive)
    {
        var axis = 0f;
        if (Input.GetKeyDown(negative))
        {
            axis -= 1f;
        }
        if (Input.GetKeyDown(positive))
        {
            axis += 1f;
        }
        return axis;
    }

    public static Vector2 GetInputDirection()
    {
        var direction = new Vector2();
        if (Input.GetKey(KeyCode.A))
        {
            direction.x -= 1f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction.x += 1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction.y -= 1f;
        }
        if (Input.GetKey(KeyCode.W))
        {
            direction.y += 1f;
        }
        return direction.normalized;
    }

    public static Vector3 AverageVector3(IEnumerable<Vector3> vectors)
    {
        var addedVector = Vector3.zero;
        int vectorCount = 0;
        foreach (var vector in vectors)
        {
            vectorCount++;
            addedVector += vector;
        }
        return addedVector / vectorCount;
    }

    public static RaycastHit AverageRaycastHit(IEnumerable<RaycastHit> hits, System.Func<RaycastHit, bool> validateHit = null)
    {
        var addedPoint = Vector3.zero;
        var addedNormal = Vector3.zero;
        int hitCount = 0;
        foreach (var hit in hits)
        {
            hitCount++;
            if (validateHit == null || validateHit.Invoke(hit))
            {
                addedPoint += hit.point;
                addedNormal += hit.normal;
            }
        }
        var averageHit = new RaycastHit();
        averageHit.point = addedPoint / hitCount;
        averageHit.normal = addedNormal / hitCount;
        return averageHit;
    }
} // end of class
} // end of namespace
