﻿#if FDZ_UNITY_ENGINE_U2D_PIXEL_PERFECT_CAMERA
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class PixelPerfectCameraZoom : MonoBehaviour
	{
		UnityEngine.U2D.PixelPerfectCamera ppc;

		public bool usePlayerPrefs = true;
		[Space]
		public int minZoom = 4;
		public int maxZoom = 64;
		public int zoomStep = 4;

		private void Start()
		{
			ppc = GetComponent<UnityEngine.U2D.PixelPerfectCamera>();
			if (usePlayerPrefs)
			{
				if (PlayerPrefs.HasKey("assetsPPU"))
				{
					ppc.assetsPPU = PlayerPrefs.GetInt("assetsPPU");
				}
			}
		}

		private void OnApplicationQuit()
		{
			if (usePlayerPrefs)
			{
				PlayerPrefs.SetInt("assetsPPU", ppc.assetsPPU);
			}
		}

		private void Update()
		{
			var sign = 0;
			if (Input.GetKeyDown(KeyCode.Minus))
			{
				sign = -1;
			}
			else if (Input.GetKeyDown(KeyCode.Equals))
			{
				sign = 1;
			}
			if (sign != 0)
			{
				var newZoom = ppc.assetsPPU + Mathf.Abs(zoomStep) * sign;
				SetZoom(newZoom);
			}
		}

		private void SetZoom(int zoom)
		{
			zoom = Mathf.Clamp(zoom, Mathf.Abs(minZoom), Mathf.Abs(maxZoom));
			zoom = Utils.NearestMultiple(zoom, zoomStep);
			ppc.assetsPPU = zoom;
		}
	} // end of class
} // end of namespace
#endif