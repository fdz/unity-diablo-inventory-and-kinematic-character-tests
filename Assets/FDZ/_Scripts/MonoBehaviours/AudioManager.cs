﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class AudioManager : MonoBehaviour
	{
		#region SINGLETON
		static AudioManager _singleton;
		public static AudioManager singleton
		{
			get
			{
				if (_singleton == null)
				{
					_singleton = FindObjectOfType<AudioManager>();
				}
				return _singleton;
			}
		}
		#endregion

		[SerializeField] Camera cam;
		AudioSource template;

		private void Awake()
		{
			template = GetComponent<AudioSource>();
			if (cam == null)
			{
				cam = Camera.main;
			}
		}

		public void PlayAtPoint(AudioClip clip, UnityEngine.Audio.AudioMixerGroup output, Vector3 position, float volume, float pitch, float minDistance, float maxDistance)
		{
			var ad = GetAudioSource3D();
			ad.transform.position = position;
			ad.clip = clip;
			ad.outputAudioMixerGroup = output;
			ad.volume = volume;
			ad.pitch = pitch < 0 ? 1 : pitch;
			ad.minDistance = minDistance;
			ad.maxDistance = maxDistance;
			ad.Play();
			Destroy(ad.gameObject, ad.clip.length / ad.pitch);
		}

		private void LateUpdate()
		{
			gameObject.name = "AudioManager.childCount=" + transform.childCount;	
		}

		private AudioSource GetAudioSource3D()
		{
			var go = new GameObject("OneShot");
			go.transform.parent = this.transform;
			var ad = go.AddComponent<AudioSource>();
			ad.spatialize = true;
			ad.spatialBlend = 1f;
			ad.playOnAwake = false;
			if (template != null)
			{
				ad.rolloffMode = template.rolloffMode;
				ad.SetCustomCurve(AudioSourceCurveType.CustomRolloff, template.GetCustomCurve(AudioSourceCurveType.CustomRolloff));
				ad.SetCustomCurve(AudioSourceCurveType.ReverbZoneMix, template.GetCustomCurve(AudioSourceCurveType.ReverbZoneMix));
				ad.SetCustomCurve(AudioSourceCurveType.SpatialBlend, template.GetCustomCurve(AudioSourceCurveType.SpatialBlend));
				ad.SetCustomCurve(AudioSourceCurveType.Spread, template.GetCustomCurve(AudioSourceCurveType.Spread));
			}
			return ad;
		}
	} // end of class
} // end of namespace