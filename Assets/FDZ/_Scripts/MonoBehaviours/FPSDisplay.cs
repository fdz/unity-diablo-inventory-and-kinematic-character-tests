﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	/// <summary>
	/// Reference: https://wiki.unity3d.com/index.php/FramesPerSecond
	/// </summary>
	public class FPSDisplay : MonoBehaviour
	{
		public int fontSize = 100;
		public Color color = new Color(0.0f, 0.0f, 0.5f, 1.0f);
		public TextAnchor anchor;
		public Vector2 offset;

		float deltaTime = 0.0f;

		void Update()
		{
			deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		}

		void OnGUI()
		{
			var w = Screen.width;
			var h = Screen.height;

			GUIStyle style = new GUIStyle();
			style.alignment = anchor;
			style.fontSize = fontSize;
			style.normal.textColor = color;

			float msec = deltaTime * 1000.0f;
			float fps = 1.0f / deltaTime;
			string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
			
			var size = style.CalcSize(new GUIContent(text));
			Rect rect = new Rect(offset.x, offset.y, size.x, size.y); // Upper Left

			if (anchor == TextAnchor.UpperCenter)
			{
				rect.x = w / 2 - size.x / 2;
				rect.y = offset.y;
			}
			else if (anchor == TextAnchor.UpperRight)
			{
				rect.x = w - size.x - offset.x;
				rect.y = offset.y;
			}
			else if (anchor == TextAnchor.MiddleLeft)
			{
				rect.x = offset.x;
				rect.y = h / 2 - size.y / 2 + offset.y;
			}
			else if (anchor == TextAnchor.MiddleCenter)
			{
				rect.x = w / 2 - size.x / 2;
				rect.y = h / 2 - size.y / 2 + offset.y;
			}
			else if (anchor == TextAnchor.MiddleRight)
			{
				rect.x = w - size.x + offset.x;
				rect.y = h / 2 - size.y / 2 + offset.y;
			}
			else if (anchor == TextAnchor.LowerLeft)
			{
				rect.x = offset.x;
				rect.y = h - size.y - offset.y;
			}
			else if (anchor == TextAnchor.LowerCenter)
			{
				rect.x = w / 2 - size.x / 2;
				rect.y = h - size.y - offset.y;
			}
			else if (anchor == TextAnchor.LowerRight)
			{
				rect.x = w - size.x - offset.x;
				rect.y = h - size.y - offset.y;
			}
			
			GUI.Label(rect, text, style);
		}
	} // end of class
} // end of namespace