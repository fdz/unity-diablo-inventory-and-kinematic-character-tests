﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class RigidbodyFrictionXY : MonoBehaviour
	{
		public float amount = 5f;
		Rigidbody rb;

		private void Awake()
		{
			rb = GetComponent<Rigidbody>();
		}

		private void FixedUpdate()
		{
			amount = Mathf.Max(amount, 0f);
			rb.AddForce(new Vector3(-rb.velocity.x * amount, 0f, -rb.velocity.z * amount));
		}
	}
}