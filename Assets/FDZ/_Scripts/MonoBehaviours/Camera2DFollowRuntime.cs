﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.InEditMode
{
	[ExecuteInEditMode]
	public class Camera2DFollowRuntime : MonoBehaviour
	{
		private void Awake()
		{
			if (Application.isEditor && Application.isPlaying)
			{
				Destroy(this);
			}
		}
		private void LateUpdate()
		{
			if (Application.isEditor && !Application.isPlaying)
			{
				var cameraFollow = GetComponent<Camera2DFollow>();
				if (cameraFollow != null && cameraFollow.target != null)
				{
					transform.position = new Vector3(
						cameraFollow.target.transform.position.x, cameraFollow.target.transform.position.y, transform.position.z);
				}
			}
		}
	} // end of class
} // end of namespace
