﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class Camera2DFollow : MonoBehaviour
	{
		static Camera2DFollow _singleton;
		public static Camera2DFollow singleton
		{
			get
			{
				if (_singleton == null)
				{
					_singleton = FindObjectOfType<Camera2DFollow>();
				}
				return _singleton;
			}
		}
		public Transform target;
		public Vector2 offset;
		public float acceleration;
		private void Update()
		{
			if (target == null)
				return;
			Vector2 position = Vector2.Lerp(transform.position, (Vector2)target.position + offset, Time.deltaTime * acceleration);
			transform.position = new Vector3(position.x, position.y, transform.position.z);
		}
	} // end of class
} // end of namespace