﻿using UnityEngine;
using UnityEditor;
namespace FDZ.Attributes.Drawers
{
    // https://forum.unity.com/threads/read-only-fields.68976/
    // https://gist.github.com/LotteMakesStuff/c0a3b404524be57574ffa5f8270268ea#file-readonlypropertydrawer-cs
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }
    }
}