﻿using UnityEngine;

namespace UnityEngine
{
    public class DontEditInPlayMode : PropertyAttribute { }
}
