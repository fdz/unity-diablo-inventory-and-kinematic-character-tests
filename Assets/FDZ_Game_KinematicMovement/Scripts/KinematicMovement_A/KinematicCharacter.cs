﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

// https://www.youtube.com/watch?v=98MBwtZU2kk&feature=youtu.be&t=480

//var v3 = mainCollider.ClosestPoint(nextPosition);
//Debug.DrawRay(v3, Vector3.up, Color.white);

namespace FDZ.Game.KinematicMovement_A
{
    // FIX: When lading and moving down and slope bounces. Reproduce: jump on slope and when near landing move down.
    public class KinematicCharacter : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField, DontEditInPlayMode] private Collider mainCollider = null;
        [SerializeField, ReadOnly] private Rigidbody rb = null;

        [Header("Input")]
        public Vector3 moveDirection;
        public bool jump;

        [Header("Movement")]
        [SerializeField] private float acceleration = 2f;
        [SerializeField] private float friction = 1f;
        [SerializeField] private float gravity = -20f;
        [SerializeField] private float jumpImpulse = 10f;
        [Space()]
        [SerializeField, ReadOnly] private Vector3 velocity;
        [SerializeField, ReadOnly] private float velocityMagnitude;
        [SerializeField, ReadOnly] private Vector3 velocityGravity;

        [Header("Grounded")]
        [SerializeField] private int maxGroundAngle = 46;
        [Space()]
        [SerializeField, ReadOnly] private bool isGrounded;
        [SerializeField, ReadOnly] private bool isOnSteepSlope;
        [SerializeField, ReadOnly] private int groundAngle;
        [SerializeField, ReadOnly] private int groundPenetrations;
        [SerializeField, ReadOnly] private bool hitCenter;
        [SerializeField, ReadOnly] private bool hitFront;

        [Header("Collisions")]
        [SerializeField, ReadOnly] private Collider[] overlapResults = new Collider[16];

        private void Awake()
        {
            mainCollider.isTrigger = true;
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        }

        public void FixedUpdate()
        {
            Vector3 nextPosition = transform.position;

            nextPosition = Gravity(nextPosition);
            nextPosition = Velocity(nextPosition);

            velocityMagnitude = velocity.magnitude;
            rb.MovePosition(nextPosition);

            // FRICTION
            velocity -= velocity * (Time.deltaTime * friction);

            // ROTATION
            Vector3 lookDirection = new Vector3(moveDirection.x, 0f, moveDirection.z).normalized;
            if (lookDirection != Vector3.zero)
            {
                Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector2.up);
                rb.MoveRotation(lookRotation);
            }
        }

        private int SphereOverlap(Vector3 nextPosition)
        {
            var capsule = mainCollider as CapsuleCollider;
            return Physics.OverlapSphereNonAlloc(nextPosition + capsule.center, capsule.height, overlapResults);
        }

        private int CapsuleOverlap(Vector3 nextPosition)
        {
            var capsule = mainCollider as CapsuleCollider;
            var halfCenter = capsule.center * .5f;
            return Physics.OverlapCapsuleNonAlloc(nextPosition + halfCenter, nextPosition - halfCenter, capsule.radius, overlapResults);
        }

        private Vector3 GetMoveDirection(Vector3 nextPosition)
        {
            Vector3 moveDirection = this.moveDirection;
            hitCenter = false;
            hitFront = false;
            if (isGrounded && moveDirection.sqrMagnitude != 0)
            {
                // ADJUST MOVE DIRECTION TO SLOPE
                float distance = 2f;
                Ray rayCenter = new Ray(nextPosition + Vector3.up, Vector3.down);
                Ray rayFront = new Ray(rayCenter.origin + moveDirection * (mainCollider as CapsuleCollider).radius, rayCenter.direction);

                //Debug.DrawRay(rayCenter.origin, rayCenter.direction * distance, Color.red);
                //Debug.DrawRay(rayFront.origin, rayFront.direction * distance, Color.red);

                RaycastHit rayHitCenter;
                bool c = Physics.Raycast(rayCenter, out rayHitCenter, distance);

                RaycastHit rayHitFront;
                bool f = Physics.Raycast(rayFront, out rayHitFront, distance);

                if (c)
                {
                    float angle = Mathf.RoundToInt(Vector3.Angle(rayHitCenter.normal, Vector2.up));
                    if (angle < maxGroundAngle)
                    {
                        hitCenter = true;
                    }
                }

                if (c && f)
                {
                    float angle = Mathf.RoundToInt(Vector3.Angle(rayHitFront.normal, Vector2.up));
                    if (angle < maxGroundAngle)
                    {
                        hitFront = true;
                        moveDirection = (rayHitFront.point - rayHitCenter.point).normalized;
                    }
                }
            }
            else if (isOnSteepSlope)
            {
                // DISABLE MOVEMENT ON STEEP SLOPE
                moveDirection = Vector3.zero;
            }
            return moveDirection;
        }

        private Vector3 Velocity(Vector3 nextPosition)
        {
            Vector3 movementStep = GetMoveDirection(nextPosition) * (Time.deltaTime * acceleration);
            velocity += movementStep;
            nextPosition += velocity * Time.deltaTime; // the prediction makes it so the character don't push each other
            int overlaps = CapsuleOverlap(nextPosition);
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                {
                    float angle = Mathf.FloorToInt(Vector3.Angle(direction, Vector2.up));
                    //if (angle > maxGroundAngle - 1)
                    if (hitCenter && hitFront == false || angle > maxGroundAngle - 1)
                        direction.y = 0;
                    Vector3 penetrationVector = direction * distance;
                    nextPosition += penetrationVector;
                    Vector3 velocityProjected = Vector3.Project(velocity, -direction);
                    velocity -= velocityProjected;
                    Debug.DrawLine(transform.position, nextPosition);
                }
            }
            return nextPosition;
        }

        private Vector3 Gravity(Vector3 nextPosition)
        {
            Vector3 gravityStep = Vector3.up * (Time.deltaTime * gravity);
            velocityGravity += gravityStep;
            if (jump)
            {
                jump = false;
                velocityGravity.y = jumpImpulse;
            }
            groundPenetrations = 0;
            nextPosition += velocityGravity * Time.deltaTime;
            int overlaps = CapsuleOverlap(nextPosition);
            //bool wasGrounded = isGrounded;
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                {
                    groundPenetrations++;
                    isGrounded = false;
                    groundAngle = Mathf.RoundToInt(Vector3.Angle(direction, Vector2.up));
                    if (groundAngle < maxGroundAngle)
                    {
                        Debug.Log("grounded");
                        isGrounded = true;
                        isOnSteepSlope = false;

                        //direction.y = direction.y - direction.x - direction.z;
                        direction.y = direction.y + System.Math.Sign(direction.y) * (Mathf.Abs(direction.x) + Mathf.Abs(direction.z));
                        direction.x = direction.z = 0f;
                        Vector3 penetrationVector2 = direction * distance;
                        nextPosition += penetrationVector2;
                        velocityGravity = Vector3.zero;
                        break;
                    }
                    isOnSteepSlope = true;
                    Vector3 penetrationVector = direction * distance;
                    nextPosition += penetrationVector;
                    Vector3 velocityProjected = Vector3.Project(velocityGravity, -direction);
                    velocityGravity -= velocityProjected;
                }
            }
            if (groundPenetrations == 0)
            {
                velocityGravity.x = velocityGravity.z = 0f;
                isGrounded = false;
                isOnSteepSlope = false;
            }
            return nextPosition;
        }
    }
}