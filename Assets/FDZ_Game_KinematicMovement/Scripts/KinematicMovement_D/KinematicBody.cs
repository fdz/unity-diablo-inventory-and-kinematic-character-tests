﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game.KinematicMovement_D
{
	public class KinematicBody : MonoBehaviour
	{
        public LayerMask layerMask = (LayerMask)1;
        public QueryTriggerInteraction queryTriggerInteraction = (QueryTriggerInteraction)1;

        [SerializeField, ReadOnly] protected Vector3 velocity;
        [SerializeField, ReadOnly] protected float velocityMagnitude;
        [SerializeField, ReadOnly] private Collider[] _overlapResults = new Collider[16];

        private Rigidbody _rb;
        private CapsuleCollider _capsuleCollider;

        public Rigidbody rb { get => _rb; private set => _rb = value; }

        public CapsuleCollider capsuleCollider { get => _capsuleCollider; private set => _capsuleCollider = value; }

        public Collider[] overlapResults { get => _overlapResults; }

        protected virtual void Awake()
        {
            capsuleCollider = GetComponent<CapsuleCollider>();
            // capsuleCollider.isTrigger = true;
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        }

        public virtual void FixedTick()
		{
            Vector3 translation = velocity * Time.deltaTime;
            Vector3 nextPosition = transform.position + translation;
            int overlaps = Overlap(translation);
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != capsuleCollider && ComputePenetration(nextPosition, otherCollider, out Vector3 direction, out float distance))
                {
                    nextPosition = OnPenetration(nextPosition, otherCollider, direction, distance);
                }
            }
            velocityMagnitude = velocity.magnitude;
            rb.MovePosition(nextPosition);
        }

        protected virtual bool ComputePenetration(Vector3 nextPosition, Collider otherCollider)
        {
            return ComputePenetration(nextPosition, otherCollider, out Vector3 direction, out float distance);
        }

        protected virtual bool ComputePenetration(Vector3 nextPosition, Collider otherCollider, out Vector3 direction, out float distance)
        {
            return Physics.ComputePenetration(capsuleCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out direction, out distance);
        }

        protected virtual Vector3 OnPenetration(Vector3 nextPosition, Collider otherCollider, Vector3 direction, float distance)
        {
            Vector3 penetrationVector = direction * distance;
            nextPosition += penetrationVector;
            Vector3 velocityProjected = Vector3.Project(velocity, -direction);
            velocity -= velocityProjected;
            return nextPosition;
        }

        protected int Overlap(Vector3 offset)
        {
            //return OverlapCapsule(overlapResults, offset);
            return OverlapBounds(overlapResults, offset);
        }

        protected int OverlapBounds(Collider[] results, Vector3 offset)
        {
            return Physics.OverlapBoxNonAlloc(capsuleCollider.bounds.center + offset, capsuleCollider.bounds.extents, results, Quaternion.identity, layerMask, queryTriggerInteraction);
        }

        protected int OverlapCapsule(Collider[] results, Vector3 offset, float heightAux = 0f, float radiusAux = 0f)
        {
            Vector3 point0 = offset + capsuleCollider.bounds.center - transform.rotation * capsuleCollider.center;
            Vector3 point1 = point0 + transform.up * (capsuleCollider.height + heightAux);
            return Physics.OverlapCapsuleNonAlloc(point0, point1, capsuleCollider.radius + radiusAux, results);
        }
    }
}