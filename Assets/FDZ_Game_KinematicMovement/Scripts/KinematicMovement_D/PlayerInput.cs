﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game.KinematicMovement_D
{
    public class PlayerInput : KinematicBody
    {
        Transform directionHelper;

        public float acceleration = 40f;
        public float friction = 5f;
        public float jumpForce = 10;

        [SerializeField, ReadOnly] public bool jump;
        [SerializeField, ReadOnly] Vector3 moveDirection;
        [SerializeField, ReadOnly] private bool isGrounded;
        [SerializeField, ReadOnly] private bool wasGrounded;

        public bool IsGrounded { get => isGrounded; }

        protected override void Awake()
        {
            base.Awake();
            directionHelper = FindObjectOfType<SimpleCamera>().transform;
        }

        private void Update()
        {
            moveDirection = FDZ.Utils.GetMoveDirection(directionHelper);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
            }
        }

        private void FixedUpdate()
        {
            Vector3 moveDirection = isGrounded ? GetMoveDirection() : this.moveDirection;
            Debug.DrawRay(transform.position, moveDirection);
            velocity += moveDirection * acceleration * Time.deltaTime;

            wasGrounded = isGrounded;
            isGrounded = false;

            if (jump)
            {
                jump = false;
                velocity.y = jumpForce;
            }
            else if (GroundCheck())
            {
                isGrounded = true;
                velocity = Vector3.ClampMagnitude(velocity, acceleration / this.friction);
            }
            else
            {
                velocity.y -= 20f * Time.deltaTime;
            }

            FixedTick();

            float friction = this.friction * Time.deltaTime;
            velocity.x -= velocity.x * friction;
            velocity.z -= velocity.z * friction;
            if (isGrounded)
            {
                velocity.y -= velocity.y * friction;
            }

            Vector3 lookDirection = new Vector3(moveDirection.x, 0f, moveDirection.z).normalized;
            if (lookDirection != Vector3.zero)
            {
                Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector2.up);
                rb.MoveRotation(lookRotation);
            }
        }

        protected override Vector3 OnPenetration(Vector3 nextPosition, Collider otherCollider, Vector3 direction, float distance)
        {
            Vector3 penetrationVector = direction * distance;
            Vector3 velocityProjected = Vector3.Project(velocity, -direction);
            float angle = Vector3.Angle(penetrationVector, Vector3.up);
            //Debug.Log($"angle = {angle}, isGrounded = {isGrounded}, wasGrounded = {wasGrounded}");

            if (angle < 90 && (isGrounded && wasGrounded == false || isGrounded == false && wasGrounded == false))
            //if (angle < 90 && (isGrounded && wasGrounded == false || isGrounded == false && wasGrounded == false))
            {
                Debug.Log("Stabilized");
                isGrounded = true;
                nextPosition += new Vector3(0f, System.Math.Sign(direction.y) * distance, 0f);
                int option = 0;
                if (option == 0)
                {
                    // OPTION A
                    velocity.y = 0f;
                }
                else if (option == 1)
                {
                    // OPTION B
                    float m = velocity.magnitude;
                    velocity.y = 0f;
                    velocity = velocity.normalized * m;
                }
                else if (option == 2)
                {
                    // OPTION C
                    var y = Mathf.Abs(velocity.y) * Time.deltaTime;
                    velocity.y = 0f;
                    float m = velocity.magnitude;
                    velocity = velocity.normalized * (m + y * 5f);
                }
                // PROJECT VELOCITY AND REMOVE IT
                velocity -= Vector3.Project(velocity, -direction);
            }
            else
            {
                nextPosition += penetrationVector;
                velocity -= velocityProjected;
            }
            return nextPosition;
        }

        private Vector3 GetMoveDirection()
        {
            Vector3 moveDirection = this.moveDirection;
            if (moveDirection.sqrMagnitude != 0)
            {
                // ADJUST MOVE DIRECTION TO SLOPE
                float distance = 2f;
                Ray rayCenter = new Ray(transform.position + Vector3.up, Vector3.down);
                Ray rayFront = new Ray(rayCenter.origin + moveDirection * capsuleCollider.radius, rayCenter.direction);

                //Debug.DrawRay(rayCenter.origin, rayCenter.direction * distance, Color.red);
                //Debug.DrawRay(rayFront.origin, rayFront.direction * distance, Color.red);

                RaycastHit rayHitCenter;
                bool c = Physics.Raycast(rayCenter, out rayHitCenter, distance);

                RaycastHit rayHitFront;
                bool f = Physics.Raycast(rayFront, out rayHitFront, distance);

                if (c && f)
                {
                    moveDirection = (rayHitFront.point - rayHitCenter.point).normalized;
                }
            }
            return moveDirection;
        }

        private bool GroundCheck()
        {
            float radiusOffset = .1f;
            float radius = capsuleCollider.radius - radiusOffset;
            Vector3 origin = rb.position + new Vector3(0, radius - radiusOffset);
            var colliders = Physics.OverlapSphere(origin, radius, layerMask, queryTriggerInteraction);
            if (colliders.Length == 0)
            {
                return false;
            }
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i] != capsuleCollider)
                {
                    return true;
                }
            }
            return false;
        }
    }
}