﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game.KinematicMovement_D
{
    public class FloatingCapsule : MonoBehaviour
    {
        public LayerMask layerMask = (LayerMask)1;

        public float acceleration = 40f;
        public float friction = 5f;
        public float jumpForce = 10;

        private Vector3 velocity;

        [SerializeField, ReadOnly] private bool jump;
        [SerializeField, ReadOnly] private Vector3 moveDirection;
        [SerializeField, ReadOnly] private bool isGrounded;

        private Transform directionHelper;
        private Rigidbody rb;
        private CapsuleCollider capsuleCollider;

        private void Awake()
        {
            capsuleCollider = GetComponent<CapsuleCollider>();
            capsuleCollider.isTrigger = true;

            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

            directionHelper = FindObjectOfType<SimpleCamera>().transform;
        }

        private void Update()
        {
            moveDirection = FDZ.Utils.GetMoveDirection(directionHelper);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
            }
        }

        private void FixedUpdate()
        {
            Vector3 moveDirection = isGrounded ? GetMoveDirection() : this.moveDirection;
            Debug.DrawRay(transform.position, moveDirection);
            
            velocity += moveDirection * acceleration * Time.deltaTime;
            velocity.y -= 20f * Time.deltaTime;

            bool jumped = false;
            if (jump)
            {
                jumped = true;
                jump = false;
                isGrounded = false;
                velocity.y = jumpForce;
                Debug.Log("Jumped " + Time.time);
            }

            Vector3 nextPosition = transform.position + velocity * Time.deltaTime;

            if (jumped == false && isGrounded == false)
            {
                var overlaps = Physics.OverlapSphere(nextPosition, 10f, layerMask, QueryTriggerInteraction.Collide);
                foreach (var otherCollider in overlaps)
                {
                    if (otherCollider != capsuleCollider && Physics.ComputePenetration(capsuleCollider, nextPosition, transform.rotation,
                        otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                    {
                        Vector3 penetrationVector = direction * distance;
                        float angle = Vector3.Angle(penetrationVector, Vector3.up);
                        if (angle < 90)
                        {
                            isGrounded = true;
                            Debug.Log("Landed " + Time.time);
                        }
                    }
                }
            }

            if (isGrounded)
            {
                //bool firstCheck = true;
                isGrounded = false;
                var hits = Physics.RaycastAll(nextPosition + capsuleCollider.center, Vector3.down, capsuleCollider.center.y + .2f, layerMask, QueryTriggerInteraction.Collide);
                foreach (var hit in hits)
                {
                    //if (hit.collider != capsuleCollider && (firstCheck || hit.point.y >= nextPosition.y))
                    if (hit.collider != capsuleCollider && (isGrounded == false || hit.point.y >= nextPosition.y))
                    {
                        //firstCheck = false;
                        isGrounded = true;
                        nextPosition.y = hit.point.y;
                        velocity.y = 0f;
                    }
                }
            }

            rb.MovePosition(nextPosition);

            float friction = this.friction * Time.deltaTime;
            velocity.x -= velocity.x * friction;
            velocity.z -= velocity.z * friction;
            if (isGrounded)
            {
                velocity.y -= velocity.y * friction;
            }
        }
        private Vector3 GetMoveDirection()
        {
            Vector3 moveDirection = this.moveDirection;
            if (moveDirection.sqrMagnitude != 0)
            {
                // ADJUST MOVE DIRECTION TO SLOPE
                float distance = 2f;
                Ray rayCenter = new Ray(transform.position + Vector3.up, Vector3.down);
                Ray rayFront = new Ray(rayCenter.origin + moveDirection * capsuleCollider.radius, rayCenter.direction);

                Debug.DrawRay(rayCenter.origin, rayCenter.direction * distance, Color.red);
                Debug.DrawRay(rayFront.origin, rayFront.direction * distance, Color.red);

                RaycastHit rayHitCenter;
                bool c = Physics.Raycast(rayCenter, out rayHitCenter, distance);

                RaycastHit rayHitFront;
                bool f = Physics.Raycast(rayFront, out rayHitFront, distance);

                if (c && f)
                {
                    moveDirection = (rayHitFront.point - rayHitCenter.point).normalized;
                }
            }
            return moveDirection;
        }
    }
}