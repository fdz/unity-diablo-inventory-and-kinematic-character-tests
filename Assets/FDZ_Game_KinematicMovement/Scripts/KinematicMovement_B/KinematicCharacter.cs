﻿using FDZ.Game.X;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game.KinematicMovement_B
{
    public class KinematicCharacter : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField, DontEditInPlayMode] private Collider mainCollider = null;
        [SerializeField, ReadOnly] private Rigidbody rb = null;

        [Header("Input")]
        public Vector3 moveDirection;
        public bool jump;
        public float maxGroundAngle = 45f;

        [Header("Properties")]
        [SerializeField] private float acceleration = 2f;
        [SerializeField] private float friction = 1f;
        [SerializeField] private float gravitySpeed = -20f;
        [SerializeField] private float jumpImpulse = 10f;
        //[SerializeField] private float maxAngle = 46f;
        [Space()]
        [SerializeField, ReadOnly] private Vector3 hvel;
        [SerializeField, ReadOnly] private float velocityMagnitude;

        //[Header("Collisions")]
        private Collider[] overlapResults = new Collider[16];

        Transform fakeParent;
        bool fakeParent_initialized;
        Quaternion fakeParent_previousRotation;
        Vector3 fakeParent_previousPosition;
        bool fakeParent_touchedOnce;
        bool isGrounded;

        Vector3 vvel;

        public void SetFakeParent(Transform fakeParent)
        {
            this.fakeParent = fakeParent;
        }

        private void Awake()
        {
            //mainCollider.isTrigger = true;
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        }

        private void Update()
        {
            moveDirection = Utils.GetMoveDirectionFromMainCamera();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
            }
        }

        private void FixedUpdate()
        {
            bool ComputePenetration(Vector3 atPosition, Collider other, out Vector3 direction, out float distance)
            {
                return Physics.ComputePenetration(
                    mainCollider,
                    atPosition,
                    transform.rotation,
                    other,
                    other.transform.position,
                    other.transform.rotation,
                    out direction,
                    out distance);
            };

            Vector3 gravityStep = Vector3.up * (Time.deltaTime * gravitySpeed);
            Vector3 movementStep = moveDirection * (Time.deltaTime * acceleration);
            hvel += movementStep;

            if (isGrounded == false)
            {
                vvel += gravityStep;
            }

            if (jump)
            {
                jump = false;
                vvel.y = jumpImpulse;
            }

            isGrounded = false;
            Vector3 nextPosition = transform.position + (hvel + vvel) * Time.deltaTime;
            int overlaps = Overlap();
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider &&
                    ComputePenetration(nextPosition, otherCollider, out Vector3 direction, out float distance))
                {
                    if (otherCollider.transform == fakeParent)
                    {
                        fakeParent_touchedOnce = true;
                    }
                    nextPosition += direction * distance;
                    hvel -= Vector3.Project(hvel, -direction);
                    var penetrationAngle = Mathf.RoundToInt(Vector3.Angle(direction, Vector3.up));
                    if (penetrationAngle > maxGroundAngle && penetrationAngle < 90f)
                    {
                        Debug.Log("ground to steep");
                        var velocityAngle = Mathf.RoundToInt(90 - Vector3.Angle(hvel.normalized, Vector3.up));
                        if (velocityAngle > 0)
                        {
                            //Debug.Log("can't walk upwards on steep ground");
                            //hvel.x = 0f;
                            //hvel.z = 0f;
                        }
                        vvel -= Vector3.Project(vvel, -direction);
                    }
                    else if (penetrationAngle <= 45f)
                    {
                        isGrounded = true;
                        vvel = Vector3.zero;
                    }
                }
            }

            velocityMagnitude = hvel.magnitude;

            Quaternion nextRotation = rb.rotation;

            if (fakeParent != null)
            {
                var mPlatform = fakeParent.GetComponent<MovingPlatform>();
                if (mPlatform != null && mPlatform.trigger.bounds.Intersects(mainCollider.bounds) == false) // TODO: dont use bounds!
                {
                    fakeParent = null;
                }
            }

            if (fakeParent != null)
            {
                if (fakeParent_initialized && fakeParent_touchedOnce)
                {
                    var rotationDifference =
                        Quaternion.AngleAxis(Utils.QuaternionSingedAngle(
                            fakeParent_previousRotation,
                            fakeParent.rotation),
                            fakeParent.up);
                    nextPosition =
                        (fakeParent.position - fakeParent_previousPosition) +
                        Utils.Vector3RotateAroundPoint(
                            nextPosition,
                            fakeParent.position,
                            rotationDifference);
                    nextRotation = Quaternion.Euler(0f, rotationDifference.eulerAngles.y, 0f) * rb.rotation;
                }
                fakeParent_previousRotation = fakeParent.rotation;
                fakeParent_previousPosition = fakeParent.position;
                fakeParent_initialized = true;
            }
            else
            {
                fakeParent_initialized = false;
                fakeParent_touchedOnce = false;
            }

            rb.MovePosition(nextPosition);

            // FRICTION
            //velocity -= velocity * (Time.deltaTime * friction);
            hvel -= new Vector3(hvel.x, 0f, hvel.z) * (Time.deltaTime * friction);

            // ROTATION
            Vector3 lookDirection = new Vector3(moveDirection.x, 0f, moveDirection.z).normalized;
            if (lookDirection != Vector3.zero)
            {
                Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector2.up);
                //rb.MoveRotation(lookRotation);
            }
            rb.MoveRotation(nextRotation);
        }

        private int Overlap()
        {
            Vector3 center = (mainCollider as CapsuleCollider).bounds.center;
            float height = (mainCollider as CapsuleCollider).height;
            return Physics.OverlapSphereNonAlloc(center, height, overlapResults, LayerMask.GetMask("Default"), QueryTriggerInteraction.Ignore);
        }
    }
}