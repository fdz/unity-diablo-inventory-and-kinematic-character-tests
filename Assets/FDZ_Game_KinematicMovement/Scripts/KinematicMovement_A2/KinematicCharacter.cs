﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

// https://www.youtube.com/watch?v=98MBwtZU2kk&feature=youtu.be&t=480

//var v3 = mainCollider.ClosestPoint(nextPosition);
//Debug.DrawRay(v3, Vector3.up, Color.white);

namespace FDZ.Game.KinematicMovement_A2
{
    // FIX: When lading and moving down and slope bounces. Reproduce: jump on slope and when near landing move down.
    public class KinematicCharacter : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField, DontEditInPlayMode] private Collider mainCollider = null;
        [SerializeField, ReadOnly] private Rigidbody rb = null;

        [Header("Input")]
        public Vector3 moveDirection;
        public bool jump;

        [Header("Movement")]
        [SerializeField] private float acceleration = 2f;
        [SerializeField] private float friction = 1f;
        [SerializeField] private float gravityForce = -20f;
        [SerializeField] private float jumpImpulse = 10f;
        [Space()]
        [SerializeField, ReadOnly] private Vector3 velocity;
        [SerializeField, ReadOnly] private float velocityMagnitude;

        [Header("Grounded")]
        [SerializeField] private int maxGroundAngle = 46;
        [SerializeField, ReadOnly] private bool isGrounded;
        [SerializeField, ReadOnly] private bool isOnSteepSlope;

        [Header("Collisions")]
        [SerializeField, ReadOnly] private Collider[] overlapResults = new Collider[16];

        private void Awake()
        {
            mainCollider.isTrigger = true;
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        }

        public void FixedUpdate()
        {
            ApplyGravity();
            rb.MovePosition(GetNextPosition());

            // FRICTION
            //velocity -= velocity * (Time.deltaTime * friction);
            var y = velocity.y;
            velocity -= velocity * (Time.deltaTime * friction);
            if (isGrounded == false)
                velocity.y = y;

            // ROTATION
            Vector3 lookDirection = new Vector3(moveDirection.x, 0f, moveDirection.z).normalized;
            if (lookDirection != Vector3.zero)
            {
                Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector2.up);
                rb.MoveRotation(lookRotation);
            }
        }

        private int SphereOverlap(Vector3 nextPosition)
        {
            var capsule = mainCollider as CapsuleCollider;
            return Physics.OverlapSphereNonAlloc(nextPosition + capsule.center, capsule.height, overlapResults);
        }

        private int CapsuleOverlap(Vector3 nextPosition, float radiusAux = 0f)
        {
            var capsule = mainCollider as CapsuleCollider;
            Vector3 point0 = capsule.bounds.center - transform.rotation * capsule.center;
            Vector3 point1 = point0 + transform.up * capsule.height;
            return Physics.OverlapCapsuleNonAlloc(point0, point1, capsule.radius + radiusAux, overlapResults);
        }

        private Vector3 GetMoveDirection(Vector3 nextPosition)
        {
            Vector3 moveDirection = this.moveDirection;
            //bool hitCenter = false;
            //bool hitFront = false;
            //if (isGrounded && moveDirection.sqrMagnitude != 0)
            //if (moveDirection.sqrMagnitude != 0)
            if (isOnSteepSlope == false && moveDirection.sqrMagnitude != 0)
                {
                // ADJUST MOVE DIRECTION TO SLOPE
                float distance = 2f;
                Ray rayCenter = new Ray(nextPosition + Vector3.up, Vector3.down);
                Ray rayFront = new Ray(rayCenter.origin + moveDirection * (mainCollider as CapsuleCollider).radius, rayCenter.direction);

                Debug.DrawRay(rayCenter.origin, rayCenter.direction * distance, Color.red);
                Debug.DrawRay(rayFront.origin, rayFront.direction * distance, Color.red);

                RaycastHit rayHitCenter;
                bool c = Physics.Raycast(rayCenter, out rayHitCenter, distance);

                RaycastHit rayHitFront;
                bool f = Physics.Raycast(rayFront, out rayHitFront, distance);

                if (c)
                {
                    float angle = Mathf.RoundToInt(Vector3.Angle(rayHitCenter.normal, Vector2.up));
                    if (angle < maxGroundAngle)
                    {
                        //hitCenter = true;
                    }
                }

                if (c && f)
                {
                    float angle = Mathf.RoundToInt(Vector3.Angle(rayHitFront.normal, Vector2.up));
                    if (angle < maxGroundAngle)
                    {
                        //hitFront = true;
                        moveDirection = (rayHitFront.point - rayHitCenter.point).normalized;
                    }
                }
            }
            Debug.DrawRay(nextPosition, moveDirection);
            return moveDirection;
        }

        private void ApplyGravity()
        {
            Vector3 g = Vector3.up * gravityForce * Time.deltaTime;

            Vector3 position = transform.position + Vector3.down * 0.01f;

            int overlaps = CapsuleOverlap(position, -0.01f);
            isGrounded = false;
            isOnSteepSlope = false;

            for (int i = 0; i < overlaps && isGrounded == false && isOnSteepSlope == false; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, position, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                {
                    int auxAngle = Mathf.RoundToInt(Vector3.Angle(direction, Vector2.up));
                    if (auxAngle < maxGroundAngle)
                    {
                        isGrounded = true;
                    }
                    else
                    {
                        isOnSteepSlope = true;
                    }
                }
            }

            if (isGrounded == false)
            {
                velocity += g;
            }
        }

        private Vector3 GetNextPosition()
        {
            Vector3 nextPosition = transform.position;

            if (jump)
            {
                jump = false;
                velocity.y = jumpImpulse;
            }

            velocity += GetMoveDirection(nextPosition) * (Time.deltaTime * acceleration);
            //velocity += moveDirection * (Time.deltaTime * acceleration);
            velocityMagnitude = velocity.sqrMagnitude;
            if (velocityMagnitude <= .001f)
            {
                velocity = Vector3.zero;
            }

            nextPosition += velocity * Time.deltaTime;

            int overlaps = CapsuleOverlap(nextPosition);
            int penetrations = 0;
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                {
                    penetrations++;

                    nextPosition += direction * distance;
                    velocity -= Vector3.Project(velocity, -direction);

                    int auxAngle = Mathf.RoundToInt(Vector3.Angle(direction, Vector2.up));
                    if (auxAngle < maxGroundAngle)
                    {
                        //isGrounded = true;
                        //isOnSteepSlope = false;
                        //groundAngle = auxAngle;
                    }
                }
            }

            if (penetrations == 0)
            {
                //isGrounded = false;
                //isOnSteepSlope = false;
            }

            Debug.DrawRay(nextPosition, velocity, Color.green);

            return nextPosition;
        }
    }
}