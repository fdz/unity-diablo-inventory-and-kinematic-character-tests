﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Game.KinematicMovement_A2
{
    public class KinematicCharacterPlayerInput : MonoBehaviour
    {
        public Transform directionHelper;
        public KinematicCharacter kc;

        private void Update()
        {
            kc.moveDirection = FDZ.Utils.GetMoveDirection(directionHelper);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                kc.jump = true;
            }
        }
    }

}