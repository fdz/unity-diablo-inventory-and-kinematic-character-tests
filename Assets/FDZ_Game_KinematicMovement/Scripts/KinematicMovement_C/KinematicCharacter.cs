﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Game.KinematicMovement_C
{
	public class KinematicCharacter : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField, DontEditInPlayMode] private Collider mainCollider = null;
        [SerializeField, ReadOnly] private Rigidbody rb = null;

        [Header("Input")]
        public Vector3 moveDirection;
        public bool jump;

        [Header("Properties")]
        [SerializeField] private float acceleration = 40f;
        [SerializeField] private float friction = 5f;
        [SerializeField] private float gravityForce = -20f;
        [SerializeField] private float jumpImpulse = 10f;
        [Space()]
        //[SerializeField] private float maxAngle = 46f;
        [Space()]
        [SerializeField, ReadOnly] private Vector3 velocity;
        [SerializeField, ReadOnly] private float velocityMagnitude;

        [Header("Collisions")]
        [SerializeField, ReadOnly] private Collider[] overlapResults = new Collider[16];
        private void Awake()
        {
            mainCollider.isTrigger = true;
            rb = GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.interpolation = RigidbodyInterpolation.Interpolate;
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        }

        private void FixedUpdate()
        {
            Vector3 nextPosition;
            int overlaps;

            Vector3 moveDirection = GetMoveDirection();
            velocity += moveDirection * (Time.deltaTime * acceleration);

            bool isGrounded = false;

            if (jump)
            {
                jump = false;
                velocity.y = jumpImpulse;
            }
            else
            {
                nextPosition = rb.position + velocity * Time.deltaTime + Vector3.down * .01f; // the prediction makes it so the character don't push each other
                overlaps = CapsuleOverlap(nextPosition, -.01f, -.01f);

                for (int i = 0; i < overlaps; i++)
                {
                    Collider otherCollider = overlapResults[i];
                    if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                    {
                        float angle = Mathf.FloorToInt(Vector3.Angle(direction, Vector2.up));
                        //if (angle < maxAngle)
                        {
                            isGrounded = true;
                            break;
                        }
                    }
                }
            }

            if (isGrounded == false)
            {
                velocity += Vector3.up * gravityForce * Time.deltaTime;
            }

            Debug.DrawRay(transform.position, Vector3.up, Color.blue);

            nextPosition = rb.position + velocity * Time.deltaTime; // the prediction makes it so the character don't push each other
            overlaps = CapsuleOverlap(nextPosition);

            Debug.DrawRay(nextPosition, Vector3.up, Color.cyan);
            
            for (int i = 0; i < overlaps; i++)
            {
                Collider otherCollider = overlapResults[i];
                if (otherCollider != mainCollider && Physics.ComputePenetration(mainCollider, nextPosition, transform.rotation, otherCollider, otherCollider.transform.position, otherCollider.transform.rotation, out Vector3 direction, out float distance))
                {
                    float angle = Mathf.FloorToInt(Vector3.Angle(direction, Vector2.up));
                    
                    Vector3 penetrationVector = direction * distance;
                    nextPosition += penetrationVector;
                    
                    Vector3 velocityProjected = Vector3.Project(velocity, -direction);
                    velocity -= velocityProjected;
                }
            }

            var y = velocity.y;
            velocity -= velocity * (Time.deltaTime * friction);
            if (isGrounded == false)
                velocity.y = y;

            rb.MovePosition(nextPosition);
            Debug.DrawRay(transform.position, Vector3.up, Color.blue);
        }

        private int CapsuleOverlap(Vector3 nextPosition, float heightAux = 0f, float radiusAux = 0f)
        {
            var capsule = mainCollider as CapsuleCollider;
            Vector3 point0 = capsule.bounds.center - transform.rotation * capsule.center;
            Vector3 point1 = point0 + transform.up * (capsule.height + heightAux);
            return Physics.OverlapCapsuleNonAlloc(point0, point1, capsule.radius + radiusAux, overlapResults);
        }
        private Vector3 GetMoveDirection()
        {
            Vector3 moveDirection = this.moveDirection;
            if (moveDirection.sqrMagnitude != 0)
            {
                // ADJUST MOVE DIRECTION TO SLOPE
                float distance = 2f;
                Ray rayCenter = new Ray(transform.position + Vector3.up, Vector3.down);
                Ray rayFront = new Ray(rayCenter.origin + moveDirection * (mainCollider as CapsuleCollider).radius, rayCenter.direction);

                Debug.DrawRay(rayCenter.origin, rayCenter.direction * distance, Color.red);
                Debug.DrawRay(rayFront.origin, rayFront.direction * distance, Color.red);

                RaycastHit rayHitCenter;
                bool c = Physics.Raycast(rayCenter, out rayHitCenter, distance);

                RaycastHit rayHitFront;
                bool f = Physics.Raycast(rayFront, out rayHitFront, distance);

                if (c && f)
                {
                    moveDirection = (rayHitFront.point - rayHitCenter.point).normalized;
                }
            }
            Debug.DrawRay(transform.position, moveDirection);
            return moveDirection;
        }
    }
}