﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Inventory
{
	[ExecuteInEditMode]
	public class InventoryItemGUI_TextMask : MonoBehaviour
	{
		public RectOffset padding = new RectOffset();

		private void OnEnable()
		{
			DrivenRectTransformTracker dt = new DrivenRectTransformTracker();
			dt.Clear();
			dt.Add(this, transform as RectTransform, DrivenTransformProperties.AnchoredPosition | DrivenTransformProperties.SizeDelta);
		}

		private void LateUpdate()
		{
			var parent = transform.parent as RectTransform;
			var rectTransform = transform as RectTransform;

			var itemRotation = Mathf.Abs(Mathf.RoundToInt(parent.transform.eulerAngles.z % 360f / 90f) * 90f);
			var rotated = (itemRotation == 90 || itemRotation == 270);
			var x = rotated ? parent.sizeDelta.y : parent.sizeDelta.x;
			var y = rotated ? parent.sizeDelta.x : parent.sizeDelta.y;
			
			rectTransform.sizeDelta = new Vector2(x - padding.left - padding.right, y - padding.top - padding.bottom);
			transform.rotation = Quaternion.identity;
		}
	}
}