﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace FDZ.Inventory
{
	public enum InventoryItemRotations { Euler0, Euler90, Euler180, Euler270 }
	public class InventoryItem
	{
		public static Vector2Int GetPositionFromRect(RectInt rect, float rotation)
		{
			var r = Mathf.RoundToInt(rotation % 360f / 90f) * 90f;
			switch (r)
			{
				case 0:
					return rect.position;
				case -270:
				case 90:
					return new Vector2Int(rect.xMin, rect.yMax);
				case -180:
				case 180:
					return new Vector2Int(rect.xMax, rect.yMax);
				case -90:
				case 270:
					return new Vector2Int(rect.xMax, rect.yMin);
				default:
					Debug.LogError("wrong rotation " + rotation + " " + r);
					break;
			}
			return new Vector2Int();
		}

		public static RectInt ComputeRect(Vector2Int position, float rotation, Vector2Int size)
		{
			var r = Mathf.RoundToInt(rotation % 360f / 90f) * 90f;
			switch (r)
			{
				case 0:
					return new RectInt(position, size);
				case -270:
				case 90:
					return new RectInt(new Vector2Int(position.x, position.y - size.x), new Vector2Int(size.y, size.x));
				case -180:
				case 180:
					return new RectInt(new Vector2Int(position.x - size.x, position.y - size.y), size);
				case -90:
				case 270:
					return new RectInt(new Vector2Int(position.x - size.y, position.y), new Vector2Int(size.y, size.x));
				default:
					Debug.LogError("wrong rotation " + rotation + " " + r);
					break;
			}
			return new RectInt();
		}

		public Inventory inventory;
		public string shortName;
		public Vector2Int position;
		public float rotation;
		public Vector2Int size;

		// https://escapefromtarkov.gamepedia.com/Backpacks
		// https://escapefromtarkov.gamepedia.com/Chest_rigs
		// https://escapefromtarkov.gamepedia.com/Containers
		public Dictionary<Vector2Int, Inventory> subInventories; // position (top left corner), inventory

		public RectInt CurrentRect { get => ComputeRect(position, rotation, size); }

		public bool Contains(Inventory inventory)
		{
			return subInventories != null && subInventories.ContainsValue(inventory);
		}

		public void RemoveFromCurrentInventory()
		{
			if (inventory != null)
			{
				inventory.Remove(this);
			}
		}
	}
}