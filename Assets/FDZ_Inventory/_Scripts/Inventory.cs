﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace FDZ.Inventory
{
	public class Inventory
	{
		public Vector2Int size;

		public List<InventoryItem> items = new List<InventoryItem>();

		public void Clear()
		{
			items.Clear();
		}

		public bool HasItem(InventoryItem item)
		{
			return items.Exists(x => x == item);
		}

		public bool CanPlace(InventoryItem item, Vector2Int position, float rotation)
		{
			var rect = InventoryItem.ComputeRect(position, rotation, item.size);
			return
				rect.position.x >= 0 &&
				rect.position.x + rect.size.x <= size.x &&
				rect.position.y >= 0 &&
				rect.position.y + rect.size.y <= size.y &&
				items.Exists(x => x.CurrentRect.Overlaps(rect)) == false &&
				item.Contains(this) == false;
		}

		public bool Place(InventoryItem item)
		{
			for (int rotation = 0; rotation < 360; rotation += 90)
			{
				for (int y = 0; y < size.y; y++)
				{
					for (int x = 0; x < size.x; x++)
					{
						if (Place(item, new Vector2Int(x, y), rotation))
						{
							return true;
						}
					}
				}
			}
			return false;
		}

		public bool Place(InventoryItem item, Vector2Int position, float rotation)
		{
			if (CanPlace(item, position, rotation) == false )
			{
				return false;
			}
			if (HasItem(item) == false)
			{
				items.Add(item);
				item.inventory = this;
			}
			item.inventory = this;
			item.position = position;
			item.rotation = rotation;
			//Debug.Log($"Inventory.Place() | item.rotation = {item.rotation}");
			return true;
		}

		public bool Remove(InventoryItem item)
		{
			if (items.Remove(item))
			{
				//Debug.Log("removed item");
				return true;
			}
			return false;
		}
	}
}