﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Game.X
{
	public class MovingPlatform : MonoBehaviour
	{
		Rigidbody rb;

		Transform target;

		public Transform start;
		public Transform end;

		public float maxSpeed = 3f;

		public Vector3 startRot;
		public Vector3 endRot;

		public float speedRot = 3f;
		float timerRot;

		public Collider trigger;

		private void Awake()
		{
			rb = GetComponent<Rigidbody>();
			target = start;
			transform.eulerAngles = startRot;
			var colliders = GetComponents<Collider>();
			trigger = System.Array.Find(colliders, c => c.isTrigger);
		}

		private void FixedUpdate()
		{
			if (start != null && end != null)
			{
				var difference = target.position - transform.position;
				var distance = difference.magnitude;
				var stepLength = Time.deltaTime * maxSpeed;
				if (stepLength > distance)
				{
					transform.position = target.position;
					if (target == start)
					{
						target = end;
					}
					else
					{
						target = start;
					}
				}
				else
				{
					rb.MovePosition(rb.position + difference.normalized * stepLength);
				}
			}
			if (speedRot > 0)
			{
				timerRot += Time.deltaTime * speedRot;
				rb.MoveRotation(Quaternion.Euler(Vector3.Lerp(startRot, endRot, Mathf.PingPong(timerRot, 1f))));
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			var mog = other.GetComponent<MovementOnGround>();
			if (mog != null)
			{
				Debug.Log("set moving paltform");
				mog.movingPlatform = rb;
			}
			var kc = other.GetComponent<FDZ.Game.KinematicMovement_B.KinematicCharacter>();
			if (kc != null)
			{
				kc.SetFakeParent(transform);
			}
		}

		private void OnTriggerExit(Collider other)
		{
			var mog = other.GetComponent<MovementOnGround>();
			if (mog != null)
			{
				Debug.Log("clear moving paltform");
				mog.momentum = rb.velocity; // keep momentum
				mog.movingPlatform = null;
			}
		}
	}
}