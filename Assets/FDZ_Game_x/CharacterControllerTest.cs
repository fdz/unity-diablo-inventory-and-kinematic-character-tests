﻿using Drawing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class CharacterControllerTest : MonoBehaviour
	{
		public Vector3 input_moveDirection;
		public bool input_jump;

		public Vector3 velocity;

		public LayerMask groundLayerMask;
		public bool isGrounded;
		public bool wasGrounded;
		public float groundRayDistanceCenter = .3f;
		public float groundRayDistanceForward = .3f;
		public float groundRayRadius = .1f;
		public float airTime;
		public float slopeLandingFix = .5f;

		private void Update()
		{
			input_moveDirection = Utils.GetMoveDirectionFromMainCamera();
			if (Input.GetKeyDown(KeyCode.Space))
			{
				input_jump = true;
			}
			
			var cc = GetComponent<CharacterController>();

			velocity = Vector3.Lerp(
					velocity,
					new Vector3(
						input_moveDirection.x * 6f,
						velocity.y,
						input_moveDirection.z * 6f),
					Time.deltaTime * 3f);
			velocity.y -= 20f * Time.deltaTime;
			if (input_jump)
			{
				input_jump = false;
				velocity.y = 10f;
			}
			var flags = cc.Move(velocity * Time.deltaTime);
			//if (flags.HasFlag(CollisionFlags.Below))
			//{
			//	velocity.y = 0f;
			//}
			return;

			var targetVelocity = input_moveDirection * 6f;
			var newVelocity = Vector3.Lerp(velocity, targetVelocity, Time.deltaTime * 3f);
			velocity = new Vector3(newVelocity.x, velocity.y, newVelocity.z);

			if (input_jump)
			{
				input_jump = false;
				velocity.y = 10f;
			}

			cc.Move(velocity * Time.deltaTime);

			var capsuleHalfHeight = cc.height / 2f;
			var capsuleCenter = transform.position + Vector3.up * capsuleHalfHeight;

			var groundPoint = Vector3.zero;
			var groundNormal = Vector3.zero;
			var groundAngle = 0f;

			if (isGrounded)
			{
				Debug.DrawRay(groundPoint, groundNormal);
				if (wasGrounded == false && (Time.time - airTime) >= slopeLandingFix)
				{
					velocity.y = 0;
				}
				bool project = true;
				if (
					Physics.Raycast(
						capsuleCenter,
						Vector3.down,
						out RaycastHit hitCenter,
						capsuleHalfHeight + groundRayDistanceCenter,
						groundLayerMask,
						QueryTriggerInteraction.Ignore)
					&&
					Physics.Raycast(
						capsuleCenter + new Vector3(velocity.x, 0f, velocity.z).normalized * groundRayRadius,
						Vector3.down,
						out RaycastHit hitForward,
						capsuleHalfHeight + groundRayDistanceForward,
						groundLayerMask,
						QueryTriggerInteraction.Ignore)
					&&
					hitCenter.collider == hitForward.collider
				)
				{
					var newDir = (hitForward.point - hitCenter.point).normalized;
					if (Mathf.Abs(90 - Vector3.Angle(newDir, Vector3.up)) < 45f)
					{
						project = false;
						velocity = newDir * velocity.magnitude;
						Draw.Ray(transform.position, velocity.normalized, Color.red);
					}
				}
				if (project)
				{
					velocity = Vector3.ProjectOnPlane(velocity, groundNormal).normalized * velocity.magnitude;
					Draw.Ray(transform.position, velocity.normalized, Color.blue);
				}
			}
			else
			{
				if (wasGrounded)
				{
					airTime = Time.time;
				}
				if (input_jump)
				{
					input_jump = false;
					velocity.y = 10f;
				}
				else
				{
					velocity.y -= 25f * Time.deltaTime;
				}
			}
		}
	}
}