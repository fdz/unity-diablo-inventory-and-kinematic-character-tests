﻿using Drawing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Game.X
{
	public class MovementOnGround : MonoBehaviour
	{
		public bool disableInput;

		public LayerMask groundLayerMask;
		public bool isGrounded;
		public bool wasGrounded;
		public float groundRayDistanceCenter = .3f;
		public float groundRayDistanceForward = .3f;
		public float groundRayRadius = .1f;
		public float airTime;
		public float slopeLandingFix = .5f;

		public float acceleration = 8f;
		public float speed = 6f;

		public Vector3 input_moveDirection;
		public bool input_jump;
		public Transform graphics;
		public float graphicsFollowSpeed = 18f;
		public Rigidbody movingPlatform;
		public bool fakeParent_initialized;
		public Quaternion fakeParent_previousRotation;
		public Vector3 fakeParent_previousPosition;
		public Vector3 velocity;
		public Vector3 momentum;
		public float momentumDeaccleration = 4f;
		public Vector3 previousPosition;
		public Quaternion previousRotation;

		public Rigidbody rb { get; private set; }

		public CapsuleCollider capsuleCollider { get; private set; }

		private void Awake()
		{
			// CAPSULE COLLIDER
			capsuleCollider = GetComponent<CapsuleCollider>();
			// RB
			rb = GetComponent<Rigidbody>();
			rb.freezeRotation = true;
			rb.useGravity = false;
			rb.interpolation = RigidbodyInterpolation.Interpolate;
			rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
			// GRAPHICS
			graphics.SetParent(null, true);
			graphics.position = transform.position;
			graphics.rotation = transform.rotation;
			previousPosition = transform.position;
			previousRotation = transform.rotation;
		}

		private void Update()
		{
			if (disableInput)
			{
				return;
			}
			input_moveDirection = Utils.GetMoveDirectionFromMainCamera();
			if (Input.GetKeyDown(KeyCode.Space))
			{
				input_jump = true;
			}
		}

		private void LateUpdate()
		{
			graphics.position = Vector3.Lerp(
				graphics.position,
				Vector3.Lerp(transform.position, previousPosition, .5f),
				Time.deltaTime * graphicsFollowSpeed);
			previousPosition = transform.position;
			graphics.rotation = Quaternion.Lerp(
				graphics.rotation,
				Quaternion.Lerp(transform.rotation, previousRotation, .5f),
				Time.deltaTime * graphicsFollowSpeed);
			previousRotation = transform.rotation;
		}

		private void FixedUpdate()
		{
			{
				var groundPoint = Vector3.zero;
				var groundNormal = Vector3.zero;
				var groundAngle = 0f;

				var capsuleCollider = GetComponent<CapsuleCollider>();
				RaycastHit rayHit;

				var capsuleHalfHeight = capsuleCollider.height / 2f;
				var capsuleCenter = transform.position + Vector3.up * capsuleHalfHeight;

				wasGrounded = isGrounded;
				isGrounded = false;
				if (Physics.SphereCast(
					capsuleCenter,
					capsuleCollider.radius - .01f,
					Vector3.down,
					out rayHit,
					capsuleHalfHeight - capsuleCollider.radius + .1f,
					groundLayerMask,
					QueryTriggerInteraction.Ignore))
				{
					groundPoint = rayHit.point;
					groundNormal = rayHit.normal;
					groundAngle = Vector3.Angle(groundNormal, Vector3.up);
					isGrounded = groundAngle < 45f;
				}

				if (input_jump)
				{
					wasGrounded = isGrounded;
					isGrounded = false;
				}

				var targetVelocity = input_moveDirection * speed;
				velocity = Vector3.Lerp(velocity, targetVelocity, Time.deltaTime * acceleration);

				if (isGrounded)
				{
					Debug.DrawRay(groundPoint, groundNormal);
					if (wasGrounded == false && (Time.time - airTime) >= slopeLandingFix)
					{
						velocity.y = 0;
					}
					bool project = true;
					if (
						Physics.Raycast(
							capsuleCenter,
							Vector3.down,
							out RaycastHit hitCenter,
							capsuleHalfHeight + groundRayDistanceCenter,
							groundLayerMask,
							QueryTriggerInteraction.Ignore)
						&&
						Physics.Raycast(
							capsuleCenter + new Vector3(velocity.x, 0f, velocity.z).normalized * groundRayRadius,
							Vector3.down,
							out RaycastHit hitForward,
							capsuleHalfHeight + groundRayDistanceForward,
							groundLayerMask,
							QueryTriggerInteraction.Ignore)
						&&
						hitCenter.collider == hitForward.collider
					){
						var newDir = (hitForward.point - hitCenter.point).normalized;
						if (Mathf.Abs(90 - Vector3.Angle(newDir, Vector3.up)) < 45f)
						{
							project = false;
							velocity = newDir * velocity.magnitude;
							Draw.Ray(rb.position, velocity.normalized, Color.red);
						}
					}
					if (project)
					{
						velocity = Vector3.ProjectOnPlane(velocity, groundNormal).normalized * velocity.magnitude;
						Draw.Ray(rb.position, velocity.normalized, Color.blue);
					}
				}
				else
				{
					if (wasGrounded)
					{
						airTime = Time.time;
					}
					velocity.y = rb.velocity.y;
					if (input_jump)
					{
						input_jump = false;
						velocity.y = 10f;
					}
					else
					{
						velocity.y -= 25f * Time.deltaTime;
					}
				}
				rb.velocity = velocity + momentum;
				momentum = Vector3.Lerp(momentum, Vector2.zero, Time.deltaTime * momentumDeaccleration);
			}
			if (movingPlatform != null)
			{
				//rb.velocity += movingPlatform.velocity;
				//rb.angularVelocity += movingPlatform.angularVelocity;
				if (fakeParent_initialized)
				{
					var rotationDifference =
						Quaternion.AngleAxis(Utils.QuaternionSingedAngle(
							fakeParent_previousRotation,
							movingPlatform.rotation),
							movingPlatform.transform.up);
					rb.position =
						(movingPlatform.position - fakeParent_previousPosition) +
						Utils.Vector3RotateAroundPoint(
							rb.position,
							movingPlatform.position,
							rotationDifference);
					rb.rotation = Quaternion.Euler(0f, rotationDifference.eulerAngles.y, 0f) * rb.rotation;
				}
				fakeParent_previousRotation = movingPlatform.rotation;
				fakeParent_previousPosition = movingPlatform.position;
				fakeParent_initialized = true;
			}
			else
			{
				fakeParent_initialized = false;
				//rb.MoveRotation(Quaternion.identity);
			}
		}
	}
}